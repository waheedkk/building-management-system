Buildings = new Meteor.Collection('buildings');

BuildingSchema = new SimpleSchema({
    
    name: {
        type: String, optional: false
    },
    address: {
        type: String, optional: false
    },
    ownerId: {
        type: String, regEx: SimpleSchema.RegEx.Id, optional: false
    },
    unit: {
        type: [Object], optional: true
    },
    'unit.$._id': {
        type: Meteor.Collection.ObjectID, optional: false 
    },
    'unit.$.unitNumber': {
        type: String, optional: false
    },
    'unit.$.squareFootage': {
        type: Number, optional: true,
    },
    'unit.$.residents': {
        type: Number, optional: true, defaultValue: 0
    },
    'unit.$.tenant': {
        type: String, regEx: SimpleSchema.RegEx.Id, optional: true
    }
});

Buildings.attachSchema( BuildingSchema );