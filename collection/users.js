Users = new Meteor.Collection('users');

UserSchema = new SimpleSchema({
    
    firstName: {
        type: String, optional: true
    },
    lastName: {
        type: String, optional: true
    },
    userName: {
        type: String, optional: true
    },
    email: {
        type: String, optional: false, regEx: SimpleSchema.RegEx.Email
    },
    phoneNumber: {
        type: String, optional: true
    },
    password: {
        type: String, optional: false
    },
    code: {
        type: String, optional: true
    },
    verified: {
        type: boolean, optional: true, defaultValue: false
    },
    owner: {
        type: boolean, optional: true, defaultValue: false
    },
    tenant: {
        type: boolean, optional: true, defaultValue: false
    },
    createdAt: {
        type: Date,
        optional: true,
        autoValue: function() {
            if (this.isInsert) {
                return new Date();
            } else if (this.isUpsert) {
                return {$setOnInsert: new Date()};
            } else {
                this.unset();  // Prevent user from supplying their own value
            }
        }
    },
    UpdatedAt: {
        type: Date,
        optional: true,
        autoValue: function() {
            if(this.isUpdate) {
                return new Date();
            }
        },
        denyInsert: true,
    }
});

Users.attachSchema( UserSchema );